import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UrgenciaListComponent } from './urgencia-list.component';

describe('UrgenciaListComponent', () => {
  let component: UrgenciaListComponent;
  let fixture: ComponentFixture<UrgenciaListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UrgenciaListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UrgenciaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
