import { Component, OnInit, ViewChild } from '@angular/core';
import { Exam } from '../../../../AppBackEnd/model/Exam';
import { ExamService } from '../examService/exam.service';

@Component({
  selector: 'app-urgencia-list',
  templateUrl: './urgencia-list.component.html',
  styleUrls: ['./urgencia-list.component.css']
})
export class UrgenciaListComponent implements OnInit {

  @ViewChild('tableConsultas') private _table: any;

  exams: Exam[] = [];
  first = 0;
  rows = 10;
  dateFilters: any;
  cols: any;

  constructor(private service: ExamService) { }

  ngOnInit(): void {
    this.getExams();

    this.cols = [
      { field: 'module', header: 'Módulo' },
      { field: 'code', header: 'Código' },
      { field: 'description', header: 'Descrição' },
      { field: 'requestDate', header: 'Data de Requisição' },
      { field: 'examDate', header: 'Data de Execução' }
    ];

    var _self = this;
    // this will be called from your templates onSelect event
    this._table.filterConstraints['dateRangeFilter'] = (value: any, filter: any): boolean => {
      // get the from/start value
      var s = _self.dateFilters[0].getTime();
      var e;
      // the to/end value might not be set
      // use the from/start date and add 1 day
      // or the to/end date and add 1 day
      if (_self.dateFilters[1]) {
        e = _self.dateFilters[1].getTime() + 86400000;
      } else {
        e = s + 86400000;
      }
      // compare it to the actual values
      return value.getTime() >= s && value.getTime() <= e;
    }
  }

  private getExams() {
    this.service.getUrgencias().subscribe(
      (items: any) => {
        this.exams = items;
      }
    );
  }

  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.exams ? this.first === (this.exams.length - this.rows) : true;
  }

  isFirstPage(): boolean {
    return this.exams ? this.first === 0 : true;
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.exams);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, "urgencias");
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

}
