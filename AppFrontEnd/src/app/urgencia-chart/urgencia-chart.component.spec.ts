import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UrgenciaChartComponent } from './urgencia-chart.component';

describe('UrgenciaChartComponent', () => {
  let component: UrgenciaChartComponent;
  let fixture: ComponentFixture<UrgenciaChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UrgenciaChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UrgenciaChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
