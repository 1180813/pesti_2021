import { Component, OnInit, ViewChild } from '@angular/core';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexTitleSubtitle
} from "ng-apexcharts";
import { Exam } from '../../../../AppBackEnd/model/Exam';
import { ExamService } from '../examService/exam.service';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  title: ApexTitleSubtitle;
};

@Component({
  selector: 'app-urgencia-chart',
  templateUrl: './urgencia-chart.component.html',
  styleUrls: ['./urgencia-chart.component.css']
})
export class UrgenciaChartComponent implements OnInit {

  @ViewChild("chart")
  chart: ChartComponent = new ChartComponent;
  public chartOptions: Partial<any>;

  exams: Exam[] = [];
  dates: any[] = [];
  newDates: any[] = [];
  chartLabels: any[] = [];
  counts: any;
  countsExtended: any;

  constructor(private service: ExamService) {
    // VALORES HARD CODED PARA EFEITOS DE DEMONSTRAÇÃO
    this.chartOptions = {
      series: [
        {
          name: "Nº de Exames",
          data: [12,9,16,23,20,23,30,31,33,39,41,48]
        }
      ],
      chart: {
        height: 579,
        type: "bar"
      },
      title: {
        text: "Número de Ocorrências em Urgência por Mês"
      },
      xaxis: {
        categories: ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"]
      }
    };
   }

  ngOnInit(): void {this.getExams();

    this.getExamsDates();
    this.getExamsDatesNewFormat();
    this.getExamsDatesAsString();

    this.counts = this.newDates.reduce((p, c) => {
      var date = c.toDateString();
      if (!p.hasOwnProperty(date)) {
        p[date] = 0;
      }
      p[date]++;
      return p;
    }, {});

    this.countsExtended = Object.keys(this.counts).map(k => {
      return { date: k, count: this.counts[k] };
    });
  }

  private getExams() {
    this.service.getConsultas().subscribe(
      (items: any) => {
        this.exams = items;
      }
    );
  }

  private getExamsDates() {
    this.exams.forEach((exam) => {
      this.dates.push(exam.getExamDate.examDate);
    })
  }

  private getExamsDatesNewFormat() {
    this.dates.forEach((date) => {
      this.newDates.push(new Date(date.getFullYear(), date.getMonth() + 1, date.getDate()));
    })
  }

  private getExamsDatesAsString() {
    this.newDates.forEach((date) => {
      if (date > new Date(2020,0,1) && date < new Date(2020,0,15)) {
        this.chartLabels.push(date.getDate() + '/' + date.getMonth() + 1 + '/' + date.getFullYear());
      }
    })
  }

}
