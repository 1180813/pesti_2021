import { Emitters } from './../emitters/emitters';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../UserService/user.service';
import { Router } from '@angular/router';
import { ImportService } from '../importService/import.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  message= '';
  auth = false;

  constructor(private userService: UserService, private router: Router, private importService: ImportService) { }

  ngOnInit(): void {
    this.userService.getUserName()
    .subscribe((res:any) => {
      this.auth = true;
      Emitters.authEmitter.emit(true);
    },
    err => {
      //window.alert('Não se encontra autenticado!')
      //this.router.navigate(['/login']);
      this.auth = false;
      Emitters.authEmitter.emit(false);
    });
  }

  importExam() {
    this.importService.importExams()
    .subscribe(() => {
      window.alert("Exames importados!");
    })
  }

}
