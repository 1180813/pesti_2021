import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InternamentoChartComponent } from './internamento-chart.component';

describe('InternamentoChartComponent', () => {
  let component: InternamentoChartComponent;
  let fixture: ComponentFixture<InternamentoChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InternamentoChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InternamentoChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
