import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

const baseUrl = 'http://localhost:3000/api/user/'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http: HttpClient) { }

  submitRegister(body: any) {
    return this._http.post(baseUrl + 'register', body, {
      observe: 'body'
    })
  }

  login(body: any) {
    return this._http.post(baseUrl + 'login', body, {
      observe: 'body',
      withCredentials: true
    })
  }

  logout() {
    return this._http.post(baseUrl + 'logout', {}, {
      withCredentials: true
    })
  }

  getUserName() {
    return this._http.get(baseUrl + 'user', {
      withCredentials: true
    });
  }
}
