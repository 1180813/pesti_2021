import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InternamentoListComponent } from './internamento-list.component';

describe('InternamentoListComponent', () => {
  let component: InternamentoListComponent;
  let fixture: ComponentFixture<InternamentoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InternamentoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InternamentoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
