import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const baseUrl = 'http://localhost:3000/api/import/'

@Injectable({
  providedIn: 'root'
})
export class ImportService {

  constructor(private _http: HttpClient) { }

  importExams() {
    return this._http.post(baseUrl, {
      observe: 'body'
    })
  }
}
