import { Emitters } from './../emitters/emitters';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../UserService/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  authenticated = false;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    Emitters.authEmitter.subscribe(
      (auth: boolean) => {
        this.authenticated = auth;
      }
    )
  }

  logout() {
    this.userService.logout()
    .subscribe(() => {
      this.authenticated = false;
    })
  }
}
