import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { ButtonModule } from 'primeng/button';
import { MultiSelectModule } from 'primeng/multiselect'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule } from 'primeng/calendar';
import { ChartModule } from 'primeng/chart';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ConsultaListComponent } from './consulta-list/consulta-list.component';
import { UrgenciaListComponent } from './urgencia-list/urgencia-list.component';
import { InternamentoListComponent } from './internamento-list/internamento-list.component';
import { ConsultaChartComponent } from './consulta-chart/consulta-chart.component';
import { InternamentoChartComponent } from './internamento-chart/internamento-chart.component';
import { UrgenciaChartComponent } from './urgencia-chart/urgencia-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    NavigationComponent,
    ConsultaListComponent,
    UrgenciaListComponent,
    InternamentoListComponent,
    ConsultaChartComponent,
    InternamentoChartComponent,
    UrgenciaChartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TableModule,
    PaginatorModule,
    BrowserAnimationsModule,
    ButtonModule,
    MultiSelectModule,
    CalendarModule,
    ChartModule,
    NgApexchartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
