import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

const baseUrl = 'http://localhost:3000/api/exam/'

@Injectable({
  providedIn: 'root'
})
export class ExamService {

  constructor(private _http: HttpClient) { }

  getExams() {
    return this._http.get(baseUrl, {
      observe: 'body'
    });
  }

  getConsultas() {
    return this._http.get(baseUrl + 'bymodule/CON', {
      observe: 'body'
    });
  }

  getInternamentos() {
    return this._http.get(baseUrl + 'bymodule/INT', {
      observe: 'body'
    });
  }

  getUrgencias() {
    return this._http.get(baseUrl + 'bymodule/URG', {
      observe: 'body'
    });
  }
}
