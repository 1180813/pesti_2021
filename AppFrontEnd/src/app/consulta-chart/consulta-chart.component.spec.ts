import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaChartComponent } from './consulta-chart.component';

describe('ConsultaChartComponent', () => {
  let component: ConsultaChartComponent;
  let fixture: ComponentFixture<ConsultaChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultaChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
