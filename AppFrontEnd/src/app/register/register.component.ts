import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../UserService/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) {
    this.form = this.formBuilder.group({
      name: '',
      email: '',
      password: ''
    });
   }

  ngOnInit(): void {
  }

  submit() {
    this.userService.submitRegister(this.form.value)
    .subscribe(() => {
      window.alert('Registado com sucesso!');
      this.router.navigate(['/login']);
    });
  }

}
