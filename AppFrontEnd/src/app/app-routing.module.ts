import { UrgenciaChartComponent } from './urgencia-chart/urgencia-chart.component';
import { InternamentoChartComponent } from './internamento-chart/internamento-chart.component';
import { ConsultaChartComponent } from './consulta-chart/consulta-chart.component';
import { RegisterComponent } from './../../../AppFrontEnd/src/app/register/register.component';
import { LoginComponent } from './../../../AppFrontEnd/src/app/login/login.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultaListComponent } from './consulta-list/consulta-list.component';
import { InternamentoListComponent } from './internamento-list/internamento-list.component';
import { UrgenciaListComponent } from './urgencia-list/urgencia-list.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'listaconsultas', component: ConsultaListComponent},
  {path: 'listaurgencias', component: UrgenciaListComponent},
  {path: 'listainternamentos', component: InternamentoListComponent},
  {path: 'graficoconsultas', component: ConsultaChartComponent},
  {path: 'graficointernamentos', component: InternamentoChartComponent},
  {path: 'graficourgencias', component: UrgenciaChartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
