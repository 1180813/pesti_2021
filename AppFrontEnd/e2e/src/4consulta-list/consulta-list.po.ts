import { browser, by, element } from 'protractor';

export class AppPage {
  
  async navigateTo(): Promise<unknown> {
    return browser.get('http://localhost:4200/listaconsultas');
  }

  async getCodeField(): Promise<string> {
    return element(by.css('[pSortableColumn="code"]')).getText();
  }

  async getDescriptonField(): Promise<string> {
    return element(by.css('[pSortableColumn="description"]')).getText();
  }

  async getRequestDateField(): Promise<string> {
    return element(by.css('[pSortableColumn="requestDate"]')).getText();
  }

  async getExamDateField(): Promise<string> {
    return element(by.css('[pSortableColumn="examDate"]')).getText();
  }

  getCodeFilterField() {
    return element(by.css('[field="code"]'));
  }

  getDescriptionFilterField() {
    return element(by.css('[field="description"]'));
  }

  getExcelDownloadButton() {
    return element(by.css('button'));
  }
}
