import { browser, logging } from 'protractor';
import { AppPage } from './consulta-list.po';

describe('Consultas List Component', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display Código field', async () => {
    await page.navigateTo();
    expect(await page.getCodeField()).toEqual('Código');
  });

  it('should display Descrição field', async () => {
    await page.navigateTo();
    expect(await page.getDescriptonField()).toEqual('Descrição');
  });

  it('should display Data de Requisição field', async () => {
    await page.navigateTo();
    expect(await page.getRequestDateField()).toEqual('Data de Requisição');
  });

  it('should display Data de Execução field', async () => {
    await page.navigateTo();
    expect(await page.getExamDateField()).toEqual('Data de Execução');
  });

  it('should display Código filter field', async () => {
    await page.navigateTo();
    expect(await page.getCodeFilterField().isDisplayed());
  });

  it('should display Descrição filter field', async () => {
    await page.navigateTo();
    expect(await page.getDescriptionFilterField().isDisplayed());
  });

  it('should display Excel download button', async () => {
    await page.navigateTo();
    expect(await page.getExcelDownloadButton().isDisplayed());
  });
});
