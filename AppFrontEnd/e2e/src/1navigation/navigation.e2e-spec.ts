import { browser, logging } from 'protractor';
import { AppPage } from './navigation.po';

describe('Navigation Bar Component', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display Home message + icon', async () => {
    await page.navigateTo();
    expect(await page.getTitleText()).toEqual('Home');
  });

  it('should display Login button', async () => {
    await page.navigateTo();
    expect(await page.getLoginButton()).toEqual('Iniciar Sessão');
  });
  
});
