import { browser, by, element } from 'protractor';

export class AppPage {
  
  async navigateTo(): Promise<unknown> {
    return browser.get('http://localhost:4200/graficoconsultas');
  }

  getChart() {
    return element(by.css('apx-chart'));
  }
}
