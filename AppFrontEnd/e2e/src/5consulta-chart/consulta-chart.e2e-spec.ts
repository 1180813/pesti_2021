import { browser, logging } from 'protractor';
import { AppPage } from './consulta-chart.po';

describe('Consultas Chart Component', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display chart', async () => {
    await page.navigateTo();
    expect(page.getChart().isDisplayed());
  });
});
