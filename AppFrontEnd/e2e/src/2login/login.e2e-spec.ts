import { browser, logging } from 'protractor';
import { AppPage } from './login.po';

describe('Login Component', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display Login message', async () => {
    await page.navigateTo();
    expect(await page.getTitle()).toEqual('Iniciar Sessão');
  });

  it('should display Email box', async () => {
    await page.navigateTo();
    expect(await page.getEmailBox()).toEqual('Email');
  });

  it('should display Password box', async () => {
    await page.navigateTo();
    expect(await page.getPasswordBox()).toEqual('Password');
  });

  it('should display Login button', async () => {
    await page.navigateTo();
    expect(await page.getLoginButton().getText()).toEqual('Entrar');
  });

  afterAll(async () => {
    let email = "migueldamiaosousa@gmail.com";
    let password = "miguel123"

    await page.getEmailBoxToInput().sendKeys(email);
    await page.getPasswordBoxToInput().sendKeys(password);
    await page.getLoginButton().click();
  })
});
