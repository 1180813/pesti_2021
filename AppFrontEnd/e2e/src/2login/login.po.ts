import { browser, by, element } from 'protractor';

export class AppPage {
  
  async navigateTo(): Promise<unknown> {
    return browser.get('http://localhost:4200/login');
  }

  async getTitle(): Promise<string> {
    return element(by.css('h1')).getText();
  }

  async getEmailBox(): Promise<string> {
    return element(by.css('[formControlName="email"]')).getAttribute("placeholder");
  }

  async getPasswordBox(): Promise<string> {
    return element(by.css('[formControlName="password"]')).getAttribute("placeholder");
  }

  getLoginButton() {
    return element(by.css('button'));
  }

  getEmailBoxToInput() {
    return element(by.css('[formControlName="email"]'));
  }

  getPasswordBoxToInput() {
    return element(by.css('[formControlName="password"]'));
  }
}
