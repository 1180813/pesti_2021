import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    email: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    password: {
        type: String,
        required: true,
        min: 6,
        max: 1024
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('User', userSchema);
/*
import { Email } from '../ValueObjects/Email';
import { Name } from '../ValueObjects/Name';
import { CreationDate } from './../ValueObjects/CreationDate';
import { Password } from './../ValueObjects/Password';

export class User {

    private name: Name;
    private email: Email;
    private password: Password;
    private date: CreationDate;

    constructor(name: Name, email: Email, password: Password, date: CreationDate) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.date = date;
    }

    get getName(): Name{
        return this.name;
    }

    get getEmail(): Email{
        return this.email;
    }

    get getPassword(): Password{
        return this.password;
    }

    get getCreationDate(): CreationDate{
        return this.date;
    }
}*/