import { ExamDate } from './../valueobjects/ExamDate';
import { RequestDate } from './../valueobjects/RequestDate';
import { Module } from './../valueobjects/Module';
import { Code } from './../valueobjects/Code';
import { Description } from './../valueobjects/Description';
import { Key } from '../valueobjects/Key';

export class ExamDTO {

    private description: Description;
    private code: Code;
    private module: Module;
    private requestDate: RequestDate;
    private examDate: ExamDate;
    private key: Key;

    constructor(desc: Description, code: Code, module: Module, rd: RequestDate, ed: ExamDate, key: Key) {
        this.description = desc;
        this.code = code;
        this.module = module;
        this.requestDate = rd;
        this.examDate = ed;
        this.key = key;
    }

    get getDescription(): Description {
        return this.description;
    }

    get getCode(): Code {
        return this.code;
    }

    get getModule(): Module {
        return this.module;
    }

    get getRequestDate(): RequestDate {
        return this.requestDate;
    }

    get getExamDate(): ExamDate {
        return this.examDate;
    }

    get getKey(): Key {
        return this.key;
    }
}