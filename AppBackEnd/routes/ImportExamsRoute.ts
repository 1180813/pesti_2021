import ImportExamsService from "../services/ImportExamsService";

const router = require('express').Router();

// POST PARA IMPORTAÇÃO
router.post('/', async (req, res) => {
    try {
        var service = new ImportExamsService();
        var importedData = service.importExams(process.env.EXAM_FILE_PATH);
        res.json(importedData).status(200);
    } catch (err) {
        res.status(400).send(err.message);
    }
});

module.exports = router;