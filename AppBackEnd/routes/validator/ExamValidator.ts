import Joi from '@hapi/joi';

// Validação de exame
const examValidation = (data) => {
    const schema = {
        exam: Joi.string()
            .max(255)
            .required(),
        code: Joi.string()
            .required(),
        module: Joi.string()
            .required()
            .valid('CON', 'URG', 'INT'),
        requestDate: Joi.date()
            .required(),
        examDate: Joi.date()
            .required(),
        key: Joi.string()
            .required()
            .unique()
    }

    return Joi.validate(data, schema);
}


module.exports.examValidation = examValidation;