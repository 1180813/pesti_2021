import { ExamService } from './../services/ExamService';
import { ExamMapper } from "../mappers/ExamMapper";

const router = require('express').Router();

// CRIAÇÃO
router.post('/', async (req, res) => {
    try {
        var examDTO = ExamMapper.jsonToDTO(req.body);

        var service = new ExamService();
        var createExamDTO = await service.create(examDTO);

        res.json(createExamDTO).status(201);
    } catch(err) {
        res.status(400).send(err.message);
    }
});

// TODOS OS EXAMES
router.get('/', async (req, res) => {
    var service = new ExamService();
    var examListDTO = await service.getAllExams();

    if(!examListDTO) {
        res.status(404).send("Não existem exames");
    } else {
        res.status(200).json(examListDTO);
    }
});

// GET PELA KEY
router.get('/bykey/:keyExpression', async (req,res) => {
    var service = new ExamService();
    var examListDTO = await service.findByKey(req);

    if(!examListDTO) {
        res.status(404).send("Exame não encontrado");
    } else {
        res.status(200).json(examListDTO);
    }
})

// GET POR MODULO
router.get('/bymodule/:moduleExpression', async (req,res) => {
    var service = new ExamService();
    var examListDTO = await service.findByModule(req);

    if(!examListDTO) {
        res.status(404).send("Exames não encontrados");
    } else {
        res.status(200).json(examListDTO);
    }
})

module.exports = router;