import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
const router = require('express').Router();
const User = require('../model/User');
const { registerValidation } = require('./validator/UserValidatior');

// REGISTO
router.post('/register', async (req, res) => {
    // validação dos dados
    const { error } = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // confirmação de emails duplicados
    const emailExists = await User.findOne({ email: req.body.email });
    if (emailExists) return res.status(400).send('O email já existe');

    // encriptação de password (hash)
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword
    });

    let promise = user.save();

    promise.then(function (doc) {
        return res.status(201).json(doc);
    });

    promise.catch(function (err) {
        return res.status(501).json({ message: 'Erro no registo' })
    });
});

// LOGIN
router.post('/login', async (req, res) => {
    // confirmar se o email existe
    const user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(404).send('O email não foi encontrado');

    // confirmar se a password está correta
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if (!validPass) return res.status(400).send('A password está errada');

    // criar e atribuir um token (JWT)
    const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
    res.cookie('jwt', token, {
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000 // 1 dia
    });

    res.send(/*"Autenticado com sucesso!"*/ {
        data: {
            token: token
        }
    });
});

router.get('/user', async (req, res) => {
    try {
        const cookie = req.cookies['jwt'];

        const claims: any = jwt.verify(cookie, process.env.TOKEN_SECRET);
        if (!claims) {
            return res.status(401).send({
                message: 'Não autenticado'
            });
        }

        const user = await User.findOne({ _id: claims._id });

        const { password, ...data } = await user.toJSON();

        res.send(data);
    } catch(err) {
        return res.status(401).send('Não autenticado');
    }
});

router.post('/logout', (req, res) => {
    res.cookie('jwt', '', {
        maxAge: 0
    });

    res.send('Sucesso');
})

module.exports = router;