import { Date } from "mongoose";

export class RequestDate {
    public requestDate: Date;

    constructor(requestDate: Date) {
        this.requestDate = requestDate;
    }

    get getRequestDate(): Date {
        return this.requestDate;
    }
}