export class Key {
    public key: String;

    constructor(key: String) {
        this.key = key;
    }

    get getKey(): String {
        return this.key;
    }
}