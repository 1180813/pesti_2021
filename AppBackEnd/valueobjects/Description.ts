export class Description {
    public description: String;

    constructor(description: String) {
        this.description = description;
    }

    get getDescription(): String {
        return this.description;
    }
}