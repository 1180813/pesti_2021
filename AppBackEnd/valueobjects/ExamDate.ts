import { Date } from "mongoose";

export class ExamDate {
    public examDate: Date;

    constructor(examDate: Date) {
        this.examDate = examDate;
    }

    get getExamDate(): Date {
        return this.examDate;
    }
}