export class Code {
    public code: String;

    constructor(code: String) {
        this.code = code;
    }

    get getCode(): String {
        return this.code;
    }
}