import * as mongoose from 'mongoose';
import { Request } from "express";
import { ExamSchema } from './../schemas/ExamSchema';
import { Exam } from '../model/Exam';
import { ExamMapper } from '../mappers/ExamMapper';

const examS = mongoose.model('Exam', ExamSchema);

export class ExamRepository {

    public async create(exam: Exam) {
        var examSchema = ExamMapper.domainToPersistence(exam);
        var createdExamSchema = await examSchema.save();
        var createdExam = ExamMapper.persistenceToDomain(createdExamSchema);

        return createdExam;
    }

    public async getAllExams() {
        var examSchema = await examS.find({});
        if (!examSchema) return null;

        var examList = ExamMapper.persistenceListToDomain(examSchema);
        return examList;
    }

    public async findByKey(req: Request) {
        var key = req.params.keyExpression;
        var condition = key ? { key: { $regex: new RegExp(key) } } : {};
        var examSchemaList = examS.findOne(condition);

        if (!examSchemaList) return null;

        return examSchemaList;
    }

    public async findByModule(req: Request) {
        var examSchema = await examS.find({ module: req.params.moduleExpression });
        if (!examSchema) return null;

        var examList = ExamMapper.persistenceListToDomain(examSchema);
        return examList;
    }
}