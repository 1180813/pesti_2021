import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import cors from 'cors';
import cookieParser from 'cookie-parser';

require('dotenv/config')

const port = process.env.PORT;
const app = express();

app.use(bodyParser.json());
app.use(cors({
  credentials: true,
  origin: ['http://localhost:4200']
}));
app.use(cookieParser());

// POSTERIORMENTE USAR ESTA MANEIRA PARA CHAMAR O RESTO DAS ROUTES
const userRoute = require('./routes/UserRoutes');
app.use('/api/user', userRoute);

const examRoute = require('./routes/ExamRoutes');
app.use('/api/exam', examRoute);

const importRoute = require('./routes/ImportExamsRoute');
app.use('/api/import', importRoute);

app.get('/', (req, res) => {
  res.send('Home Page!')
});

mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true });

app.listen(port, () => console.log('Server up and running!'));


