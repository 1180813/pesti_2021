import { ExamService } from './ExamService';
import excelToJson from 'convert-excel-to-json';
import { ExamMapper } from '../mappers/ExamMapper';
import { ExamDTO } from '../DTOs/ExamDTO';

export default class ImportExamsService {
    
    constructor() {
    }

    public async importExams(xlsxFile: any) {

        var service = new ExamService();
        
        const excelData = excelToJson({
            sourceFile: xlsxFile,
            sheets:[{
                // Excel Sheet Name
                name: 'report',
                // Header Row -> be skipped and will not be present at our result object.
                header:{
                   rows: 1
                },
                // Mapping columns to keys
                columnToKey: {
                    A: 'description',
                    B: 'code',
                    C: 'module',
                    D: 'requestDate',
                    E: 'examDate',
                    F: 'key'
                }
            }]
        });
        
        for(let i = 0; i < excelData.report.length; i++) {
            var examDTO = ExamMapper.xlsxToDTO(excelData.report[i]);
            await service.create(examDTO as ExamDTO);
        }
    }
}