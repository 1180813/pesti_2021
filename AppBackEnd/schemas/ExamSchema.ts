import mongoose from 'mongoose';

export const ExamSchema = new mongoose.Schema({
    description: {
        type: String,
        required: true,
        max: 255
    },
    code: {
        type: String,
        required: true
    },
    module: {
        type: String,
        required: true,
        enum: ['CON','URG','INT']
    },
    requestDate: {
        type: Date,
        required: true
    },
    examDate: {
        type: Date,
        required: true
    },
    key: {
        type: String,
        required: true
    }
});

//module.exports = mongoose.model('Exam', examSchema);