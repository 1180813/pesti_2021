import * as mongoose from 'mongoose';
import { MongooseDocument } from "mongoose";
import { ExamDTO } from '../DTOs/ExamDTO';
import { Exam } from '../model/Exam';
import { ExamSchema } from '../schemas/ExamSchema';

export abstract class ExamMapper {
    
    static domainToPersistence(exam: Exam) {
        var ExamS = mongoose.model('Exam', ExamSchema);
        var examSchema = new ExamS({
            description: exam.getDescription,
            code: exam.getCode,
            module: exam.getModule,
            requestDate: exam.getRequestDate,
            examDate: exam.getExamDate,
            key: exam.getKey
        });
        console.log(examSchema);

        var error = examSchema.validateSync();

        if(error) throw new Error("Exame inválido");

        return examSchema;
    }

    static persistenceToDomain(examSchema: any) {
        var examJsonString = JSON.stringify(examSchema);
        var parseJson = JSON.parse(examJsonString);

        return new Exam(parseJson.description, parseJson.code, parseJson.module, parseJson.requestDate, parseJson.examDate, parseJson.key);
    }

    static persistenceListToDomain(examSchemaList: any /*Array<MongooseDocument>*/) {
        return examSchemaList.map((examSchema: any) => this.persistenceToDomain(examSchema));
    }

    static DTOtoDomain(examDTO: ExamDTO) {
        return new Exam(examDTO.getDescription, examDTO.getCode, examDTO.getModule, examDTO.getRequestDate, examDTO.getExamDate, examDTO.getKey);
    }

    static domainToDTO(exam: Exam) {
        return new ExamDTO(exam.getDescription, exam.getCode, exam.getModule, exam.getRequestDate, exam.getExamDate, exam.getKey);
    }

    static domainListToDTO(examList: Array<Exam>) {
        return examList.map(exam => this.domainToDTO(exam));
    }

    static jsonToDTO(exam: MongooseDocument) {
        var examJsonString = JSON.stringify(exam);
        var parseJson = JSON.parse(examJsonString);

        return new ExamDTO(parseJson.description, parseJson.code, parseJson.module, parseJson.requestDate, parseJson.examDate, parseJson.key);
    }

    static xlsxToDTO(jsonObject: Object) {
        var examJsonString = JSON.stringify(jsonObject);
        var parseJson = JSON.parse(examJsonString);
        return new ExamDTO(parseJson.description, parseJson.code, parseJson.module, parseJson.requestDate, parseJson.examDate, parseJson.key);
    }
}